<?php

namespace Drupal\xtcsearch\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

/**
 * Plugin implementation of the xtc_handler for Search.
 *
 */
abstract class SearchBase extends XtcHandlerPluginBase {

  const TIMEOUT = 5;

  /**
   * @var
   */
  protected $client;

  /**
   * @var array
   */
  protected $params;

  /**
   * @var array
   */
  protected $definition;

  /**
   * @var array
   */
  protected $request = [];

  protected function initProcess() {
    $this->buildClient();
    $this->setParams();
  }

  protected function getTimeout() {
    return self::TIMEOUT;
  }

  protected function runProcess() {
    $query = \Drupal::request()->query;
    $qs = $query->all();
    $this->params = array_merge($this->params, $this->options, $qs);
    $this->cleanParams();
  }

  protected function cleanParams() {
  }

  protected function setParams() {
    $this->params = [];
  }

  protected function buildClient() {
    $this->client = null;
  }

}
