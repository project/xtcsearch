<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtcsearch\XtendedContent\API;


use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtcsearch\XtendedContent\Interfaces\UnindexInterface;

/**
 * Class UnindexItem
 *
 * @package Drupal\xtcsearch\XtendedContent\API
 */
class UnindexItem
{

  /**
   * @param $name
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase|void
   */
  public static function delete($name, $options = []){
    $handler = XtcLoaderHandler::get($name, $options);
    if (!empty($handler) && ($handler instanceof UnindexInterface)) {
      return $handler->deleteContent();
    }
    return;
  }

}
