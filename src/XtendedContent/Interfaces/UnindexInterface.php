<?php

namespace Drupal\xtcsearch\XtendedContent\Interfaces;


use Drupal\xtc\XtendedContent\Interfaces\DeleteInterface;

interface UnindexInterface extends DeleteInterface {

}
